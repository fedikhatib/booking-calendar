import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:table_calendar/table_calendar.dart' as tc
    show StartingDayOfWeek;

import '../core/booking_controller.dart';
import '../model/booking_service.dart';
import '../model/enums.dart' as bc;
import '../util/booking_util.dart';
import 'booking_dialog.dart';
import 'booking_explanation.dart';
import 'booking_slot.dart';
import 'common_button.dart';
import 'common_card.dart';

class BookingCalendarMain extends StatefulWidget {
  const BookingCalendarMain({
    Key? key,
    required this.getBookingStream,
    required this.convertStreamResultToDateTimeRanges,
    required this.uploadBooking,
    this.navigationCallback,
    this.bookingExplanation,
    this.bookingGridCrossAxisCount,
    this.bookingGridChildAspectRatio,
    this.formatDateTime,
    this.bookingButtonText,
    this.bookingButtonColor,
    this.bookedSlotColor,
    this.selectedSlotColor,
    this.availableSlotColor,
    this.bookedSlotText,
    this.bookedSlotTextStyle,
    this.selectedSlotText,
    this.selectedSlotTextStyle,
    this.availableSlotText,
    this.availableSlotTextStyle,
    this.gridScrollPhysics,
    this.loadingWidget,
    this.errorWidget,
    this.uploadingWidget,
    this.wholeDayIsBookedWidget,
    this.pauseSlotColor,
    this.pauseSlotText,
    this.hideBreakTime = false,
    this.locale,
    this.startingDayOfWeek,
    this.disabledDays,
    this.disabledDates,
    this.lastDay,
  }) : super(key: key);

  final Stream<dynamic>? Function(
      {required DateTime start, required DateTime end}) getBookingStream;
  final Future<dynamic> Function({required BookingService newBooking})
      uploadBooking;
        final Function?
      navigationCallback;
  final List<DateTimeRange> Function({required dynamic streamResult})
      convertStreamResultToDateTimeRanges;

  ///Customizable
  final Widget? bookingExplanation;
  final int? bookingGridCrossAxisCount;
  final double? bookingGridChildAspectRatio;
  final String Function(DateTime dt)? formatDateTime;
  final String? bookingButtonText;
  final Color? bookingButtonColor;
  final Color? bookedSlotColor;
  final Color? selectedSlotColor;
  final Color? availableSlotColor;
  final Color? pauseSlotColor;

//Added optional TextStyle to available, booked and selected cards.
  final String? bookedSlotText;
  final String? selectedSlotText;
  final String? availableSlotText;
  final String? pauseSlotText;

  final TextStyle? bookedSlotTextStyle;
  final TextStyle? availableSlotTextStyle;
  final TextStyle? selectedSlotTextStyle;

  final ScrollPhysics? gridScrollPhysics;
  final Widget? loadingWidget;
  final Widget? errorWidget;
  final Widget? uploadingWidget;

  final bool? hideBreakTime;
  final DateTime? lastDay;
  final String? locale;
  final bc.StartingDayOfWeek? startingDayOfWeek;
  final List<int>? disabledDays;
  final List<DateTime>? disabledDates;

  final Widget? wholeDayIsBookedWidget;

  @override
  State<BookingCalendarMain> createState() => _BookingCalendarMainState();
}

class _BookingCalendarMainState extends State<BookingCalendarMain> {
  late BookingController controller;
  final now = DateTime.now();

  @override
  void initState() {
    super.initState();
    controller = context.read<BookingController>();
    final firstDay = calculateFirstDay();

    startOfDay = firstDay.startOfDayService(controller.serviceOpening!);
    endOfDay = firstDay.endOfDayService(controller.serviceClosing!);
    _focusedDay = firstDay;
    _selectedDay = firstDay;
    controller.selectFirstDayByHoliday(startOfDay, endOfDay);
  }

  CalendarFormat _calendarFormat = CalendarFormat.month;

  late DateTime _selectedDay;
  late DateTime _focusedDay;
  late DateTime startOfDay;
  late DateTime endOfDay;

  void selectNewDateRange() {
    startOfDay = _selectedDay.startOfDayService(controller.serviceOpening!);
    endOfDay = _selectedDay
        .add(const Duration(days: 1))
        .endOfDayService(controller.serviceClosing!);

    controller.base = startOfDay;
    controller.resetSelectedSlot();
  }

  DateTime calculateFirstDay() {
    final now = DateTime.now();
    if (widget.disabledDays != null) {
      return widget.disabledDays!.contains(now.weekday)
          ? now.add(Duration(days: getFirstMissingDay(now.weekday)))
          : now;
    } else {
      return DateTime.now();
    }
  }

  int getFirstMissingDay(int now) {
    for (var i = 1; i <= 7; i++) {
      if (!widget.disabledDays!.contains(now + i)) {
        return i;
      }
    }
    return -1;
  }

  @override
  Widget build(BuildContext context) {
    controller = context.watch<BookingController>();

    return Consumer<BookingController>(
      builder: (_, controller, __) => Padding(
        padding: const EdgeInsets.symmetric(horizontal: 0, vertical: 0),
        child: (controller.isUploading) 
            ? widget.uploadingWidget ?? const BookingDialog()
            : Column(
                children: [
                  CommonCard(
                    child: TableCalendar(
                      availableGestures: AvailableGestures.none,

                        availableCalendarFormats: const {
                                 CalendarFormat.month: 'Mois'
                                },
                      headerStyle: const HeaderStyle(/* formatButtonDecoration:  const BoxDecoration(border: const Border.fromBorderSide(BorderSide()), borderRadius: const BorderRadius.all(Radius.circular(12.0)), color:Colors.white) , */
                    titleTextStyle: TextStyle(color: Colors.white),
                      formatButtonVisible: false, // Hide the header format button
          leftChevronIcon: Icon(    // Customize the left (previous) arrow
            Icons.arrow_back,
            color: Colors.red,      // Change the color of the left arrow
          ),
          rightChevronIcon: Icon(   // Customize the right (next) arrow
            Icons.arrow_forward,
            color: Colors.red,     // Change the color of the right arrow
          ),
         titleCentered: true,
           ),
                      startingDayOfWeek: widget.startingDayOfWeek?.toTC() ??
                          tc.StartingDayOfWeek.monday,
                         
                      holidayPredicate: (day) {
                        if (widget.disabledDates == null) return false;

                        bool isHoliday = false;
                        for (var holiday in widget.disabledDates!) {
                          if (isSameDay(day, holiday)) {
                            isHoliday = true;
                          }
                        }
                        return isHoliday;
                      },
                      enabledDayPredicate: (day) {
                        if (widget.disabledDays == null &&
                            widget.disabledDates == null) return true;

                        bool isEnabled = true;
                        if (widget.disabledDates != null) {
                          for (var holiday in widget.disabledDates!) {
                            if (isSameDay(day, holiday)) {
                              isEnabled = false;
                            }
                          }
                          if (!isEnabled) return false;
                        }
                        if (widget.disabledDays != null) {
                          isEnabled =
                              !widget.disabledDays!.contains(day.weekday);
                        }

                        return isEnabled;
                      },
                      locale: widget.locale,
                      firstDay: calculateFirstDay(),
                      lastDay: widget.lastDay ??
                          DateTime.now().add(const Duration(days: 1000)),
                      focusedDay: _focusedDay,
                      calendarFormat: _calendarFormat,
                      calendarStyle:
                          const CalendarStyle(isTodayHighlighted: true ,disabledTextStyle: TextStyle(color: Colors.grey),
                  outsideTextStyle: TextStyle(color: Colors.amber),
                  holidayTextStyle: TextStyle(color: Colors.pink),
                  weekendTextStyle: TextStyle(color: Colors.white),

                  outsideDaysVisible: false,
                  defaultTextStyle: TextStyle(color: Colors.white),),
                      selectedDayPredicate: (day) {
                        return isSameDay(_selectedDay, day);
                      },
                      onDaySelected: (selectedDay, focusedDay) {
                        if (!isSameDay(_selectedDay, selectedDay)) {
                          setState(() {
                            _selectedDay = selectedDay;
                            _focusedDay = focusedDay;
                            
                             print("setstate table ${ controller.selectedSlotm}");
                             // we need to implement Map<int,List<int>> for selectedSlotm
                             // int = selected calendar day
                             //List<int> = selected slots hours

                          });
                          selectNewDateRange();
                        }
                      },
                      onFormatChanged: (format) {
                        if (_calendarFormat != format) {
                          setState(() {
                            _calendarFormat = format;
                          });
                        }
                      },
                      onPageChanged: (focusedDay) {
                        _focusedDay = focusedDay;
                      },
                    ),
                  ),
                  const SizedBox(height: 8),
                  widget.bookingExplanation ??
                      Wrap(
                        alignment: WrapAlignment.spaceAround,
                        spacing: 8.0,
                        runSpacing: 8.0,
                        direction: Axis.horizontal,
                        children: [
                          BookingExplanation(
                              color: widget.availableSlotColor ??
                                  Colors.greenAccent,
                              text: widget.availableSlotText ?? "Available"),
                          BookingExplanation(
                              color: widget.selectedSlotColor ??
                                  Colors.orangeAccent,
                              text: widget.selectedSlotText ?? "Selected"),
                          BookingExplanation(
                              color: widget.bookedSlotColor ?? Colors.redAccent,
                              text: widget.bookedSlotText ?? "Booked"),
                          if (widget.hideBreakTime != null &&
                              widget.hideBreakTime == false)
                            BookingExplanation(
                                color: widget.pauseSlotColor ?? Colors.grey,
                                text: widget.pauseSlotText ?? "Break"),
                        ],
                      ),
                  const SizedBox(height: 8),
                  StreamBuilder<dynamic>(
                    stream: widget.getBookingStream(
                        start: startOfDay, end: endOfDay),
                    builder: (context, snapshot) {
                      if (snapshot.hasError) {
                        return widget.errorWidget ??
                            Center(
                              child: Text(snapshot.error.toString()),
                            );
                      }

                      if (!snapshot.hasData) {
                        return widget.loadingWidget ??
                            const Center(child: CircularProgressIndicator());
                      }

                      ///this snapshot should be converted to List<DateTimeRange>
                      final data = snapshot.requireData;
                      controller.generateBookedSlots(
                          widget.convertStreamResultToDateTimeRanges(
                              streamResult: data));

                      return Expanded(
                        child: (widget.wholeDayIsBookedWidget != null &&
                                controller.isWholeDayBooked())
                            ? widget.wholeDayIsBookedWidget!
                            : GridView.builder(
                                physics: widget.gridScrollPhysics ??
                                    const BouncingScrollPhysics(),
                                itemCount: controller.allBookingSlots.length,
                                itemBuilder: (context, index) {
                                  TextStyle? getTextStyle() {
                                    if (controller.isSlotBooked(index)) {
                                      return widget.bookedSlotTextStyle;
                                    } else if (
                                        controller.selectedSlotm.contains(index)) {
                                      return widget.selectedSlotTextStyle;
                                    } else {
                                      return widget.availableSlotTextStyle;
                                    }
                                  }

                                  final slot = controller.allBookingSlots
                                      .elementAt(index);
                                  return BookingSlot(
                                    hideBreakSlot: widget.hideBreakTime,
                                    pauseSlotColor: widget.pauseSlotColor,
                                    availableSlotColor:
                                        widget.availableSlotColor,
                                    bookedSlotColor: widget.bookedSlotColor,
                                    selectedSlotColor: widget.selectedSlotColor,
                                    isPauseTime:
                                        controller.isSlotInPauseTime(slot),
                                    isBooked: controller.isSlotBooked(index),
                                    isSelected:
                                        controller.selectedSlotm.contains(index),
                                    onTap: () => controller.selectSlot(index),
                                    child: Center(
                                      child: Text(
                                        widget.formatDateTime?.call(slot) ??
                                            BookingUtil.formatDateTime(slot),
                                        style: getTextStyle(),
                                      ),
                                    ),
                                  );
                                },
                                gridDelegate:
                                    SliverGridDelegateWithFixedCrossAxisCount(
                                  crossAxisCount:
                                      widget.bookingGridCrossAxisCount ?? 3,
                                  childAspectRatio:
                                      widget.bookingGridChildAspectRatio ?? 1.5,
                                ),
                              ),
                      );
                    },
                  ),
                  const SizedBox(
                    height: 16,
                  ),
                /*   CommonButton(
                    text: widget.bookingButtonText ?? 'Réserver',
                    onTap: () async {
                      controller.toggleUploading();
                      await widget.uploadBooking(
                          newBooking:
                              controller.generateNewBookingForUploading());
                      controller.toggleUploading();
                      controller.resetSelectedSlot();
                    },
                    isDisabled: controller.selectedSlot == -1,
                    buttonActiveColor: widget.bookingButtonColor,
                  ), */
                  MaterialButton(
                    disabledColor: controller.selectedSlot != -1 ? Colors.grey : null,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(15)),
                    
                    onPressed: controller.selectedSlot == -1 ? null : () async {

                      print("custom button printed ${controller.selectedSlot == -1}");
                      controller.toggleUploading();
                     controller.selectedSlotm.forEach((element) async{
                       await widget.uploadBooking(
                          newBooking:
                            /// The above code appears to be incomplete and contains a syntax error. It
                            /// seems to be written in the Dart programming language, but the purpose or
                            /// functionality of the code cannot be determined without the complete
                            /// code.
                              controller.generateNewBookingForUploadingm(element));
                     });
                     widget.navigationCallback!();
                     //inspect(controller.allBookingSlots);
                     controller.allBookingSlots.forEach((element) {
                       print("element ${element}");
                       
                     });
                     controller.selectedSlotm.forEach((element) {print("elementxxxx ${controller.allBookingSlots.elementAt(element)}");});
                     
                      controller.toggleUploading();
                      controller.resetSelectedSlot();
                    },
                    child:controller.bookedSlotsEmpty?Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),gradient: LinearGradient(
    colors: <Color>[HexColor.fromHex("#FF9933"), HexColor.fromHex("#FF3366")],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight) ),
    child:Text('Réserver',style: TextStyle(color: Colors.white),)
                    ): controller.selectedSlot != -1&&controller.selectedSlotm.isNotEmpty?Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(borderRadius: BorderRadius.circular(15),gradient: LinearGradient(
    colors: <Color>[HexColor.fromHex("#FF9933"), HexColor.fromHex("#FF3366")],
    begin: Alignment.centerLeft,
    end: Alignment.centerRight) ),
    child:Text('Réserver',style: TextStyle(color: Colors.white),)
                    ): Text("Sélectionner un horaire d'abord",style: TextStyle(color: Colors.white),),
                  
                   )
                ],
              ),
      ),
    );
  }
}
extension HexColor on Color {
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }}